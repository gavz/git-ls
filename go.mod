module github.com/audibleblink/git-ls

go 1.12

require (
	github.com/google/go-github/v25 v25.0.4
	golang.org/x/oauth2 v0.0.0-20190517181255-950ef44c6e07
	gopkg.in/src-d/go-git.v4 v4.12.0
)
